from django.db import models

# Create your models here.
class Content(models.Model):
    request = models.CharField(max_length=255)
    content = models.TextField()

    def __str__(self):
        return self.content