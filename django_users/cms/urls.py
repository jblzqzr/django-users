from . import views
from django.urls import path

urlpatterns = [
    path('', views.index),
    path('logout/', views.logout_view),
    path('<str:llave>/', views.contents),
]