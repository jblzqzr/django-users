from django.http import HttpResponse
from .models import Content
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.contrib.auth import logout


@csrf_exempt
def index(request):
    if request.method == "PUT":
        if request.user.is_authenticated:
            print('hello')
            try:
                put_body = request.body.decode("utf-8").split("&")
                if len(put_body) > 2:
                    return HttpResponse("Solo una petición por PUT")
                else:
                    id_recurso = put_body[0].split("=")[0]
                    recurso = put_body[0].split("=")[1]
                    id_content = put_body[1].split("=")[0]
                    content = put_body[1].split("=")[1]
                    if id_recurso == 'request' and id_content == 'content':
                        try:
                            c = Content.objects.get(request=recurso)
                            # Si existe, lo actualizo.
                            c.content = content

                        except Content.DoesNotExist:
                            c = Content(request=recurso, content=content)
                        c.save()

                    else:
                        raise Exception()

            except IndexError:
                return HttpResponse("Error en la petición")
        else:
            return HttpResponse("No tienes permisos")
    valores_columna = Content.objects.values_list('request', flat=True)
    # Convertir el queryset en una lista de valores
    lista_valores = list(valores_columna)
    print(lista_valores)
    if request.user.is_authenticated:
        check = True
        opt = "Logged in as " + request.user.username
    else:
        check = False
        opt = "Not logged in."
    return render(request, 'cms/index.html', {'val': lista_valores, 'logged': {'check': check, 'opt': opt}})


@csrf_exempt
def contents(request, llave):
    if request.method == "GET":
        print(request.path)
        try:
            contenido = Content.objects.get(request=llave)
        except Content.DoesNotExist:
            return HttpResponse("Recurso no encontrado")
        if request.user.is_authenticated:
            check = True
            opt = "Logged in as " + request.user.username
        else:
            check = False
            opt = "Not logged in."
        return render(request,  'cms/content.html', {'content': contenido, 'logged': {'check': check, 'opt': opt}})
    elif request.method == "PUT":
        if request.user.is_authenticated:
            try:
                put_body = request.body.decode("utf-8").split("&")
                if len(put_body) > 1:
                    return HttpResponse("Sobran campos en la peticion")
                else:
                    recurso = request.path.split("/", 2)[1]
                    print(recurso)
                    id_content = put_body[0].split("=")[0]
                    content = put_body[0].split("=")[1]
                    if id_content == 'content':
                        c = Content.objects.get(request=recurso)
                        # LO actualizo.
                        c.content = content
                        c.save()

                    else:
                        raise Exception()
                return HttpResponse("Recurso actualizado")
            except (IndexError, Exception):
                return HttpResponse("Error en la petición")
        else:
            return HttpResponse("Not logged in")


def logout_view(request):
    logout(request)
    return redirect("/")
